// Thêm số
var arrNums = [];
var arrSoDuong = [];
function themSo() {
    var addNumber = document.getElementById("txt-number").value * 1;
    arrNums.push(addNumber);
    document.getElementById("txt-themso").innerHTML = arrNums;

    if (addNumber > 0) {
        arrSoDuong.push(addNumber);
    }

    var valid = true; // đặt cờ hiệu kiểm tra số dương trong mảng bài 4
    valid &= kiemTraSoDuong(arrSoDuong, "so-duong-nho-nhat");
    if (!valid) {
        return;
    }
}

// Bài 1: Tính tổng các số dương
function tinhTong() {
    var sum = 0;
    for (i = 0; i < arrNums.length; i++) {
        if (arrNums[i] > 0) {
            sum += arrNums[i];
            document.getElementById("ketqua-tong").innerHTML = sum;
        }
    }
}

// Bài 2: Đếm số dương trong mảng
function demSoDuong() {
    var soduong = 0;
    for (i = 0; i < arrNums.length; i++) {
        if (arrNums[i] > 0) {
            soduong++;
            document.getElementById("demsoduong").innerHTML = "có " + soduong + " số dương";
        }
    }
}

//BÀI 3: Tìm số nhỏ nhất trong mảng
function timSoNhoNhat() {
    var minNums = arrNums[0];
    for (i = 1; i < arrNums.length; i++) {
        if (arrNums[i] < minNums) {
            minNums = arrNums[i];
        } document.getElementById("so-nho-nhat").innerHTML = `số nhỏ nhất: ${minNums}`;
    }
}

// BÀI 4: TÌM SỐ DƯƠNG NHỎ NHẤT:
/** check mảng: thêm số 0 => không hợp lệ
 * @param {*} arrSoDuong truyền mảng
 * @param {*} selectorError truyền ID
 */
function kiemTraSoDuong(arrSoDuong, selectorError) {
    if (arrSoDuong == 0) {
        document.getElementById(selectorError).innerHTML = `<p class="text-danger">số dương không hợp lệ !</p>`;
        return false;
    } document.getElementById(selectorError).innerHTML = "" + `số dương nhỏ nhất:`;
    return true;
}

// Tìm số dương nhỏ nhất:
function timSoDuongNhoNhat() {
    var soDuong = arrSoDuong[0];
    if (arrSoDuong.length === 0) {
        document.getElementById("so-duong-nho-nhat").innerHTML = "không có số dương";
        return;
    }
    for (i = 0; i < arrSoDuong.length; i++) {
        if (soDuong > arrSoDuong[i]) {
            soDuong = arrSoDuong[i];
        } document.getElementById("so-duong-nho-nhat").innerHTML = `số dương nhỏ nhất: ${soDuong}`;
    }
}

// BÀI 5: TÌM SỐ CHẴN CUỐI CÙNG

function timSoChanCuoiCung() {
    var soChanCuoiCung = 0;
    for (i = 0; i < arrNums.length; i++) {
        if (arrNums[i] % 2 == 0) {
            soChanCuoiCung = arrNums[i];
        } document.getElementById("so-chan-cuoi-cung").innerHTML = ` số chẵn cuối cùng: ${soChanCuoiCung} `;
    }
}

// BÀI 6: CHUYỂN ĐỔI VỊ TRÍ TRONG MẢNG

function doiVitri() {
    var index1 = document.getElementById("index-1").value;
    var index2 = document.getElementById("index-2").value;
    var viTri1 = arrNums[index1];
    var viTri2 = arrNums[index2];
    arrNums[index1] = viTri2;
    arrNums[index2] = viTri1;
    document.getElementById("ketquadoicho").innerHTML = arrNums;
}


// BÀI 7: SẮP XẾP TĂNG DẦN:
// function xepSoTangDan() {
//     var soTangDan = arrNums.sort();
//     document.getElementById("sap-xep").innerHTML = `mảng sau khi sắp xếp: ${soTangDan}`;
// }

function xepSoTangDan() {
    var soTangDan = arrNums[0];
    for (var i = 0; i < arrNums.length - 1; i++) {
        for (var j = i + 1; j < arrNums.length; j++) {
            if (arrNums[i] > arrNums[j]) {
                soTangDan = arrNums[i];
                arrNums[i] = arrNums[j];
                arrNums[j] = soTangDan;

            }
        }
    }
    document.getElementById("sap-xep").innerHTML = `mảng sau khi sắp xếp: ${arrNums}`;
}

//BÀI 8: ĐẾM SỐ NGUYÊN
function timSoNguyenToDauTien() {
    var soNguyenToDauTien = arrNums[0];
    for (var i = 2; i < arrNums.length; i++) {
        if (arrNums[i] < 2 && arrNums[i] % 2 == 0) {
            return false;
        } else if (arrNums[i] % 2 == 1) {
            soNguyenToDauTien = arrNums[i];
            document.getElementById("timsonguyento").innerHTML = "số nguyên tố đầu tiên: " + soNguyenToDauTien;
            break;
        }
    }
}


//<!-- BÀI 9: ĐẾM SỐ NGUYÊN -->
function demSoNguyen() {
    var count = 0;
    var soNguyen = arrNums[0];
    for (var i = 0; i < arrNums.length; i++) {
        soNguyen = arrNums[i];
        var timSoNguyen = Number.isInteger(soNguyen);
        if (timSoNguyen !== false) {
            count++;
            document.getElementById("dem-so-nguyen").innerHTML = "số nguyên: " + count;
        }
    }
}


//BÀI 10: SO SÁNH SỐ DƯƠNG VÀ SỐ ÂM
function soSanhSoAmDuong() {
    var soAm = 0;
    var soDuong = 0;
    for (var i = 0; i < arrNums.length; i++) {
        if (arrNums[i] > 0) {
            soDuong++;
        } else if (arrNums[i] < 0) {
            soAm++;
        }
        if (soDuong < soAm) {
            document.getElementById("so-sanh-so-am-duong").innerHTML = `số âm > số dương`;
        } else if (soDuong > soAm) {
            document.getElementById("so-sanh-so-am-duong").innerHTML = `số âm < số dương`;
        } else {
            document.getElementById("so-sanh-so-am-duong").innerHTML = `số âm = số dương`;
        }
    }
}


